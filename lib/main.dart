import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false,
      home: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            title: Text(
              "weather",

            ),
            leading: Icon(
              Icons.add,
              color: Colors.white,
            ),
            actions: [
              Icon(
                Icons.more_vert,
                color: Colors.white,
              )
            ],
          ),
          body: Center(
            child: Stack(
              children: <Widget>[
                Container(
                  decoration:BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage("https://img.freepik.com/free-photo/white-cloud-blue-sky_74190-7709.jpg?w=996&t=st=1672218979~exp=1672219579~hmac=032e3290e6092f930c40a815b0b49c6daf8992830b2f103e0a3b37d053145a63"),
                        fit: BoxFit.cover,
                    )
                  ) ,
                ),


                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                   Padding(padding: const EdgeInsets.all(50.0),
                   child: Icon(
                     Icons.sunny,
                     size: 200,
                     color: Colors.white,

                   ),


                   ),
                    _temp(),
                    _location(),
                    _TOday(),
                    _temprow(),
                    _tempcolum()

                  ],
                ),
                Column(

                  )

              ],
            ),
          )


      ),



    );

  }
}
_TOday(){
  return Row(
    children: [

      Text("TODAY : 27/28/2022 ",style: TextStyle(fontSize: 20,
          color: Colors.white
      ),
      ),

    ],
  );
}
_location(){
  return Row(
    children: [
      Icon(Icons.location_on,color: Colors.white,)
      ,
      SizedBox(
        width: 10,


      ),
      Text("Bangsaen,Chonburi",style: TextStyle(fontSize: 20,
          color: Colors.white
      ),
      ),

    ],
  );
}
_temp(){
  return Text("-10 ํ",style: TextStyle(fontSize: 80,
      fontWeight: FontWeight.w400,
        color: Colors.white

  ),
textAlign: TextAlign.center,
  );
}
final tims = ['29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ'] ;

_temprow(){
  return Container(
    height: 100,
    decoration: BoxDecoration(
      border: Border(
        top: BorderSide(
          color: Colors.white),
        bottom: BorderSide(color: Colors.white),
      ),

    ),
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: tims.length ,
      itemBuilder: (context,index){

      return Container(
    width: 50,
        child: Center(
          child: Text('${tims[index]}',
          style: TextStyle(color: Colors.white),),
        ),
      );
    },
    ),
  );
}
final num = ['tomorrow : sunny','2','3','1','2','3','1','2','3','1','2','3'] ;
_tempcolum(){
  return Container(
    height: 310,
    decoration: BoxDecoration(
      border: Border(
        top: BorderSide(
            color: Colors.white),
        bottom: BorderSide(color: Colors.white),
      ),

    ),
    child: ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: num.length ,
      itemBuilder: (context,index){

        return Container(
          height: 50,
          child: Center(
            child: Text('${num[index]}',
              style: TextStyle(color: Colors.white),),
          ),
        );
      },
    ),
  );

}